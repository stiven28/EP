//                        Inicializacao I2C
//..................................................................... 
Serial pc(SERIAL_TX, SERIAL_RX);
I2C i2c(PB_9, PB_8 );//SDA,SCL

//DigitalOut myled(LED1);

int main(){
    // Desativa modo de hibernação do MPU6050
    cmd[0] = 0x6B;
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);
    
    pc.printf("TESTE DE CONEXAO PARA O GIROSCOPIO E O ACELEROMETRO \n\r");
    //.....................................................................
    //        Quem sou eu para a MPU6050 (giroscópio e acelerômetro)
    //.....................................................................
    pc.printf("1. Teste de conexao da MPU6050... \n\r"); // Verifica a conexao
    cmd[0] = 0x75;
    i2c.write(MPU6050_address, cmd, 1);
    i2c.read(MPU6050_address, data, 1);
    if (data[0] != 0x68) { // DEFAULT_REGISTER_WHO_AM_I_MPU6050 0x68
      pc.printf("Erro de conexao com a MPU6050 \n\r");
      pc.printf("Opaaa. Eu nao sou a MPU6050, Quem sou eu? :S. I am: %#x \n\r",data[0]);
      pc.printf("\n\r");
      while (1);
    }else{
      pc.printf("Conexao bem sucedida com a MPU6050 \n\r");
      pc.printf("Oi, tudo joia?... Eu sou a MPU6050 XD \n\r");
      pc.printf("\n\r");
    }
    wait(0.1);  
    // Configura o Girôscopio (Full Scale Gyro Range  = 250 deg/s)
    cmd[0] = 0x1B; //GYRO_CONFIG 0x1B //Registrador de configuracao do Girôscopio
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);                //gyro full scale 250 DPS
    // Configura o Acelerômetro (Full Scale Accelerometer Range  = 2g)
    cmd[0] = 0x1C; // ACCEL_CONFIG 0x1C //Registrador de configuracao do Acelerômetro
    cmd[1] = 0x00;
    i2c.write(MPU6050_address, cmd, 2);                //ACC fullsclae 2G
    wait(0.01);
    while(1) {
    if(pc.readable()){
        if(pc.getc() == 'H'){
            for(i=0; i<100; i++){
                cmd[0]=0x3B;
                i2c.write(MPU6050_address, cmd, 1);
                i2c.read(MPU6050_address, GirAcel, 14);
                t.reset();
                t.start();
                raw_accelx = GirAcel[0] <<8 | GirAcel[1];
                raw_accely = GirAcel[2] <<8 | GirAcel[3];
                raw_accelz = GirAcel[4] <<8 | GirAcel[5];
                raw_temp = GirAcel[6] <<8 | GirAcel[7];
                raw_gyrox = GirAcel[8] <<8 | GirAcel[9];
                raw_gyroy = GirAcel[10] <<8 | GirAcel[11];
                raw_gyroz = GirAcel[12] <<8 | GirAcel[13];
                wait_us(8380);
                t.stop();
                timer = t.read();
                pc.printf("%d, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f\n", i++, cont_timer, (float)raw_accelx, (float)raw_accely, (float)raw_accelz, (float)raw_gyrox, (float)raw_gyroy, (float)raw_gyroz);
                }
            }
        }
    }
}
